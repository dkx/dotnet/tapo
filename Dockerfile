﻿FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

COPY . /App
WORKDIR /App

RUN dotnet publish src/ElectricityConsumptionToMqtt/ElectricityConsumptionToMqtt.csproj

FROM mcr.microsoft.com/dotnet/runtime:6.0

COPY --from=build /App/src/ElectricityConsumptionToMqtt/bin/Debug/net6.0/publish /App
WORKDIR /App

RUN apt update && \
	apt install -y iputils-ping net-tools

ENTRYPOINT ["dotnet"]
CMD ["/App/DKX.Tapo.ElectricityConsumptionToMqtt.dll"]
