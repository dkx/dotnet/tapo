﻿using System.Net;
using System.Net.NetworkInformation;
using DKX.Tapo.Client;
using DKX.Tapo.Client.Dates;
using DKX.Tapo.Client.Network;
using DKX.Tapo.ElectricityConsumptionToMqtt;

var tapoConnection = Utils.ReadConnectionString(Utils.RequireEnvironmentVariable("TAPO_CONNECTION"));
var mqttConnection = Utils.ReadConnectionString(Utils.RequireEnvironmentVariable("MQTT_CONNECTION"));

var debug = tapoConnection.ContainsKey("debug") && tapoConnection["debug"] == "1";
var dry = tapoConnection.ContainsKey("dry") && tapoConnection["dry"] == "1";
var span = TimeSpan.FromSeconds(5);

var cancellationTokenSource = new CancellationTokenSource();
var cancellationToken = cancellationTokenSource.Token;
Console.CancelKeyPress += (_, eventArgs) =>
{
	Console.WriteLine("Cancelling...");
	cancellationTokenSource.Cancel();
	eventArgs.Cancel = true;
};

using var mqtt = new MqttSender(mqttConnection["server"], mqttConnection["user"], mqttConnection["password"], 3);
var networkScanner = new NetworkScanner(debug);
var dateTimeProvider = new DateTimeProvider();
var http = new HttpClient(new HttpClientHandler { UseCookies = false });
var client = new TapoClient(networkScanner, dateTimeProvider, http, tapoConnection["user"], tapoConnection["password"], debug);
var runner = new Runner(dateTimeProvider, client, mqtt, span, dry);

try
{
	var devices = Environment.GetEnvironmentVariable("DEVICES");
	if (string.IsNullOrWhiteSpace(devices))
	{
		await runner.Run(cancellationToken);
	}
	else
	{
		var devicesList = Utils
			.ReadConnectionString(devices)
			.Select(item => new DeviceAddress(PhysicalAddress.Parse(item.Key), IPAddress.Parse(item.Value)))
			.ToArray();

		await runner.Run(devicesList, cancellationToken);
	}
}
finally
{
	await mqtt.Disconnect();
}
