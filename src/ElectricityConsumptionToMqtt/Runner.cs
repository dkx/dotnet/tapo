﻿using System.Diagnostics;
using System.Net.NetworkInformation;
using DKX.Tapo.Client;
using DKX.Tapo.Client.Dates;
using DKX.Tapo.Client.Devices;

namespace DKX.Tapo.ElectricityConsumptionToMqtt;

internal sealed class Runner
{
	private readonly IDateTimeProvider _dateTimeProvider;

	private readonly TapoClient _tapo;

	private readonly MqttSender _mqtt;

	private readonly TimeSpan _span;

	private readonly bool _dry;

	private readonly IDictionary<PhysicalAddress, EnergyPerHour> _energy = new Dictionary<PhysicalAddress, EnergyPerHour>();

	public Runner(IDateTimeProvider dateTimeProvider, TapoClient tapo, MqttSender mqtt, TimeSpan span, bool dry = false)
	{
		_dateTimeProvider = dateTimeProvider;
		_tapo = tapo;
		_mqtt = mqtt;
		_span = span;
		_dry = dry;
	}

	public async Task Run(IReadOnlyCollection<DeviceAddress> devices, CancellationToken cancellationToken)
	{
		Console.Write("Creating Tapo devices from provided list...");

		var tasks = devices.Select(device => _tapo.CreateDevice(device.MacAddress, device.IpAddress, cancellationToken));
		var plugs = (await Task.WhenAll(tasks))
			.Where(d => d is PlugP110)
			.Cast<PlugP110>()
			.ToArray();

		Console.WriteLine(plugs.Length > 0 ? $" created {plugs.Length} plugs" : " no plugs created");

		await DoRun(plugs, cancellationToken);
	}

	public async Task Run(CancellationToken cancellationToken)
	{
		Console.Write("Searching for Tapo devices...");

		var plugs = (await _tapo.GetDeviceList(cancellationToken))
			.Where(d => d is PlugP110)
			.Cast<PlugP110>()
			.ToArray();

		Console.WriteLine(plugs.Length > 0 ? $" found {plugs.Length} plugs" : " no plugs found");

		await DoRun(plugs, cancellationToken);
	}

	private async Task DoRun(IReadOnlyCollection<PlugP110> plugs, CancellationToken cancellationToken)
	{
		if (plugs.Count < 1)
		{
			return;
		}

		foreach (var plug in plugs)
		{
			_energy.Add(plug.MacAddress, new EnergyPerHour(_span));
		}

		if (!_dry)
		{
			await _mqtt.ConfigureAutoDiscovery(plugs, false, cancellationToken);
		}

		var timer = new PeriodicTimer(_span);
		while (!cancellationToken.IsCancellationRequested && await timer.WaitForNextTickAsync(cancellationToken))
		{
			await Loop(plugs, cancellationToken);
		}
	}

	private async Task Loop(IReadOnlyCollection<PlugP110> plugs, CancellationToken cancellationToken)
	{
		Console.Write($"{_dateTimeProvider.GetCurrentDateTime()}: Collecting energy usage from {plugs.Count} plugs...");

		var watch = Stopwatch.StartNew();

		var usage = await Task.WhenAll(plugs.Select(plug => CollectDeviceUsageAndPublish(plug, cancellationToken)));
		var totalPower = Math.Round(usage.Select(x => x.Power).Sum(), 3);
		var totalEnergy = Math.Round(usage.Select(x => x.Energy).Sum(), 3);

		if (!_dry)
		{
			await _mqtt.SendTotalState(totalPower, totalEnergy, cancellationToken);
		}

		Console.WriteLine($" {totalPower}W, {totalEnergy}kWh (took {watch.ElapsedMilliseconds}ms)");
	}

	private async Task<(double Power, double Energy)> CollectDeviceUsageAndPublish(PlugP110 plug, CancellationToken cancellationToken)
	{
		var key = await plug.Login(cancellationToken);
		var usage = await plug.GetEnergyUsage(key, cancellationToken);

		var now = _dateTimeProvider.GetCurrentDateTime();
		var power = usage.CurrentPower;
		var energy = _energy[plug.MacAddress].AddCurrentPowerAndGetEnergy(now, power);

		if (!_dry)
		{
			await _mqtt.SendPlugState(plug, power, energy, cancellationToken);
		}

		return (power, energy);
	}
}
