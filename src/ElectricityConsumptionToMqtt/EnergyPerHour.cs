﻿using FluentDateTime;

namespace DKX.Tapo.ElectricityConsumptionToMqtt;

internal sealed class EnergyPerHour
{
	private readonly IDictionary<DateTime, double> _readings = new Dictionary<DateTime, double>();

	private readonly TimeSpan _interval;

	public EnergyPerHour(TimeSpan interval)
	{
		_interval = interval;
	}

	public double AddCurrentPowerAndGetEnergy(DateTime now, double current)
	{
		var hourBeginning = now
			.SetMinute(0)
			.SetSecond(0)
			.SetMillisecond(0);

		var sum = current;
		foreach (var item in _readings)
		{
			if (item.Key < hourBeginning)
			{
				_readings.Remove(item);
			}
			else
			{
				sum += item.Value;
			}
		}

		_readings.Add(now, current);

		return sum / 1000 * _interval.Seconds / 3600;
	}
}
