﻿namespace DKX.Tapo.ElectricityConsumptionToMqtt;

internal static class Utils
{
	public static string RequireEnvironmentVariable(string name)
	{
		var value = Environment.GetEnvironmentVariable(name)?.Trim();
		if (string.IsNullOrEmpty(value))
		{
			throw new Exception($"Missing environment variable '{name}'");
		}

		return value;
	}

	public static IReadOnlyDictionary<string, string> ReadConnectionString(string connection)
	{
		return connection
			.Split(";")
			.Where(item => !string.IsNullOrWhiteSpace(item))
			.Select(item => item.Trim().Split("=").Select(part => part.Trim()).ToArray())
			.ToDictionary(parts => parts[0], parts => parts[1]);
	}
}
