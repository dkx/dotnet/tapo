﻿using System.Net;
using System.Net.NetworkInformation;

namespace DKX.Tapo.ElectricityConsumptionToMqtt;

internal sealed record DeviceAddress(PhysicalAddress MacAddress, IPAddress IpAddress);
