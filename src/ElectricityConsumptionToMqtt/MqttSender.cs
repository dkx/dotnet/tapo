﻿using System.Globalization;
using System.Text.Json;
using DKX.Tapo.Client.Devices;
using MQTTnet;
using MQTTnet.Client;

namespace DKX.Tapo.ElectricityConsumptionToMqtt;

internal sealed class MqttSender : IDisposable
{
	private readonly int _precision;

	private readonly IMqttClient _client;

	private readonly MqttClientOptions _clientOptions;

	public MqttSender(string server, string user, string password, int precision)
	{
		_precision = precision;
		_client = new MqttFactory().CreateMqttClient();
		_clientOptions = new MqttClientOptionsBuilder()
			.WithTcpServer(server)
			.WithCredentials(user, password)
			.Build();
	}

	public void Dispose()
	{
		_client.Dispose();
	}

	public async Task Disconnect()
	{
		await _client.DisconnectAsync();
	}

	public async Task ConfigureAutoDiscovery(IReadOnlyCollection<PlugP110> plugs, bool remove, CancellationToken cancellationToken)
	{
		if (!await TryConnect(cancellationToken))
		{
			throw new Exception("Can not configure auto discovery");
		}

		var tasks = new List<Task>
		{
			ConfigureTotalPowerAutoDiscovery(remove, cancellationToken),
			ConfigureTotalEnergyAutoDiscovery(remove, cancellationToken),
		};

		tasks.AddRange(plugs.Select(p => ConfigurePlugPowerAutoDiscovery(p, remove, cancellationToken)));
		tasks.AddRange(plugs.Select(p => ConfigurePlugEnergyAutoDiscovery(p, remove, cancellationToken)));

		await Task.WhenAll(tasks);
	}

	public async Task SendPlugState(PlugP110 plug, double power, double energy, CancellationToken cancellationToken)
	{
		if (!await TryConnect(cancellationToken))
		{
			return;
		}

		await SendState($"plug_{plug.MacAddress}", power, energy, cancellationToken);
	}

	public async Task SendTotalState(double power, double energy, CancellationToken cancellationToken)
	{
		if (!await TryConnect(cancellationToken))
		{
			return;
		}

		await SendState("plugs_total", power, energy, cancellationToken);
	}

	private async Task SendState(string objectId, double power, double energy, CancellationToken cancellationToken)
	{
		var powerMessage = new MqttApplicationMessageBuilder()
			.WithTopic($"homeassistant/sensor/{objectId}/state")
			.WithPayload(Math.Round(power, _precision).ToString(CultureInfo.InvariantCulture))
			.Build();

		var energyMessage = new MqttApplicationMessageBuilder()
			.WithTopic($"homeassistant/sensor/{objectId}_kwh/state")
			.WithPayload(Math.Round(energy, _precision).ToString(CultureInfo.InvariantCulture))
			.Build();

		await Task.WhenAll(
			_client.PublishAsync(powerMessage, cancellationToken),
			_client.PublishAsync(energyMessage, cancellationToken)
		);
	}

	private async Task ConfigureTotalPowerAutoDiscovery(bool remove, CancellationToken cancellationToken)
	{
		var config = new MqttApplicationMessageBuilder()
			.WithTopic("homeassistant/sensor/plugs_total/config")
			.WithRetainFlag();

		if (remove)
		{
			config = config.WithPayload("");
		}
		else
		{
			config = config.WithPayload(JsonSerializer.Serialize(new Dictionary<string, object>
			{
				["device_class"] = "power",
				["icon"] = "mdi:power-socket-fr",
				["name"] = "PlugsTotal",
				["state_class"] = "measurement",
				["state_topic"] = "homeassistant/sensor/plugs_total/state",
				["unit_of_measurement"] = "W",
			}));
		}

		await _client.PublishAsync(config.Build(), cancellationToken);
	}

	private async Task ConfigureTotalEnergyAutoDiscovery(bool remove, CancellationToken cancellationToken)
	{
		var config = new MqttApplicationMessageBuilder()
			.WithTopic("homeassistant/sensor/plugs_total_kwh/config")
			.WithRetainFlag();

		if (remove)
		{
			config = config.WithPayload("");
		}
		else
		{
			config = config.WithPayload(JsonSerializer.Serialize(new Dictionary<string, object>
			{
				["device_class"] = "energy",
				["icon"] = "mdi:power-socket-fr",
				["name"] = "PlugsTotalEnergy",
				["state_class"] = "total_increasing",
				["state_topic"] = "homeassistant/sensor/plugs_total_kwh/state",
				["unit_of_measurement"] = "kWh",
			}));
		}

		await _client.PublishAsync(config.Build(), cancellationToken);
	}

	private async Task ConfigurePlugPowerAutoDiscovery(PlugP110 plug, bool remove, CancellationToken cancellationToken)
	{
		var config = new MqttApplicationMessageBuilder()
			.WithTopic($"homeassistant/sensor/plug_{plug.MacAddress}/config")
			.WithRetainFlag();

		if (remove)
		{
			config = config.WithPayload("");
		}
		else
		{
			config = config.WithPayload(JsonSerializer.Serialize(new Dictionary<string, object>
			{
				["device"] = new Dictionary<string, object>
				{
					["hw_version"] = plug.HardwareVersion,
					["identifiers"] = plug.DeviceId,
					["manufacturer"] = "Tapo TP-Link",
					["model"] = "P110",
					["name"] = "SmartPlug",
					["sw_version"] = plug.SoftwareVersion,
				},
				["device_class"] = "power",
				["icon"] = "mdi:power-socket-fr",
				["name"] = plug.Alias,
				["object_id"] = plug.MacAddress.ToString(),
				["state_class"] = "measurement",
				["state_topic"] = $"homeassistant/sensor/plug_{plug.MacAddress}/state",
				["unique_id"] = plug.MacAddress.ToString(),
				["unit_of_measurement"] = "W",
			}));
		}

		await _client.PublishAsync(config.Build(), cancellationToken);
	}

	private async Task ConfigurePlugEnergyAutoDiscovery(PlugP110 plug, bool remove, CancellationToken cancellationToken)
	{
		var config = new MqttApplicationMessageBuilder()
			.WithTopic($"homeassistant/sensor/plug_{plug.MacAddress}_kwh/config")
			.WithRetainFlag();

		if (remove)
		{
			config = config.WithPayload("");
		}
		else
		{
			config = config.WithPayload(JsonSerializer.Serialize(new Dictionary<string, object>
			{
				["device"] = new Dictionary<string, object>
				{
					["hw_version"] = plug.HardwareVersion,
					["identifiers"] = plug.DeviceId,
					["manufacturer"] = "Tapo TP-Link",
					["model"] = "P110",
					["name"] = "SmartPlug",
					["sw_version"] = plug.SoftwareVersion,
				},
				["device_class"] = "energy",
				["icon"] = "mdi:power-socket-fr",
				["name"] = plug.Alias + " kWh",
				["object_id"] = plug.MacAddress + "_kwh",
				["state_class"] = "total_increasing",
				["state_topic"] = $"homeassistant/sensor/plug_{plug.MacAddress}_kwh/state",
				["unique_id"] = plug.MacAddress + "_kwh",
				["unit_of_measurement"] = "kWh",
			}));
		}

		await _client.PublishAsync(config.Build(), cancellationToken);
	}

	private async Task<bool> TryConnect(CancellationToken cancellationToken)
	{
		if (_client.IsConnected)
		{
			return true;
		}

		var mqttConnected = await _client.ConnectAsync(_clientOptions, cancellationToken);

		if (_client.IsConnected)
		{
			return true;
		}

		Console.WriteLine($"Could not connect to MQTT ({mqttConnected.ResultCode}): {mqttConnected.ReasonString}");
		return false;
	}
}
