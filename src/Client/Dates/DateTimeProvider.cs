﻿namespace DKX.Tapo.Client.Dates;

public sealed class DateTimeProvider : IDateTimeProvider
{
	public DateTime GetCurrentDateTime()
	{
		return DateTime.Now;
	}
}
