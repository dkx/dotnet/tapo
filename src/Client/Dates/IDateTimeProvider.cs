﻿namespace DKX.Tapo.Client.Dates;

public interface IDateTimeProvider
{
	DateTime GetCurrentDateTime();
}
