﻿using System.Net;
using System.Net.NetworkInformation;
using DKX.Tapo.Client.Data;
using DKX.Tapo.Client.Dates;

namespace DKX.Tapo.Client.Devices;

public sealed record PlugP110 : Device
{
	internal PlugP110(
		TapoClient client,
		IDateTimeProvider dateTimeProvider,
		PhysicalAddress macAddress,
		IPAddress ipAddress,
		string deviceId,
		string alias,
		string hardwareVersion,
		string softwareVersion,
		DeviceKey? key = null,
		DateTime? keyCreatedAt = null
	) : base(client, dateTimeProvider, macAddress, ipAddress, deviceId, alias, hardwareVersion, softwareVersion, key, keyCreatedAt)
	{
	}

	public async Task<EnergyUsage> GetEnergyUsage(DeviceKey deviceKey, CancellationToken cancellationToken = default)
	{
		return await Client.SecurePassthrough(IpAddress, deviceKey, new Dictionary<string, object>
		{
			["method"] = "get_energy_usage",
		}, (_, data) => Task.FromResult(new EnergyUsage(data.GetProperty("current_power").GetInt32() / 1000.0)), cancellationToken);
	}
}
