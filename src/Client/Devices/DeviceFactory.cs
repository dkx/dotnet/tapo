﻿using System.Net;
using System.Net.NetworkInformation;
using System.Text.Json;
using System.Text.RegularExpressions;
using DKX.Tapo.Client.Dates;
using DKX.Tapo.Client.Network;

namespace DKX.Tapo.Client.Devices;

internal sealed class DeviceFactory
{
	private readonly TapoClient _client;

	private readonly IDateTimeProvider _dateTimeProvider;

	public DeviceFactory(TapoClient client, IDateTimeProvider dateTimeProvider)
	{
		_client = client;
		_dateTimeProvider = dateTimeProvider;
	}

	public async Task<Device?> Create(PhysicalAddress macAddress, IPAddress ipAddress, CancellationToken cancellationToken)
	{
		var deviceKey = await _client.DeviceLogin(ipAddress, cancellationToken);
		var deviceKeyCreatedAt = _dateTimeProvider.GetCurrentDateTime();
		var deviceInfo = await _client.GetDeviceInfo(ipAddress, deviceKey, cancellationToken);

		return deviceInfo.Model switch
		{
			"P110" => new PlugP110(
				_client,
				_dateTimeProvider,
				macAddress,
				ipAddress,
				deviceInfo.DeviceId,
				deviceInfo.Nickname,
				deviceInfo.HardwareVersion,
				deviceInfo.FirmwareVersion,
				deviceKey,
				deviceKeyCreatedAt
			),
			_ => null,
		};
	}

	public async Task<Device?> Create(JsonElement data, IReadOnlyCollection<NetworkDevice> networkDevices, CancellationToken cancellationToken)
	{
		var macAddress = GetMacAddress(data);
		var networkDevice = networkDevices.FirstOrDefault(d => d.MacAddress.Equals(macAddress));

		if (networkDevice is null)
		{
			return null;
		}

		return await Create(macAddress, networkDevice.IpAddress, cancellationToken);
	}

	private static PhysicalAddress GetMacAddress(JsonElement data)
	{
		return PhysicalAddress.Parse(Regex.Replace(data.GetProperty("deviceMac").GetString()!, ".{2}", "$0:").TrimEnd(':'));
	}
}
