﻿using System.Net;
using System.Net.NetworkInformation;
using DKX.Tapo.Client.Data;
using DKX.Tapo.Client.Dates;

namespace DKX.Tapo.Client.Devices;

public abstract record Device
{
	protected readonly TapoClient Client;

	private readonly IDateTimeProvider _dateTimeProvider;

	public readonly PhysicalAddress MacAddress;

	public readonly IPAddress IpAddress;

	public readonly string DeviceId;

	public readonly string HardwareVersion;

	public readonly string SoftwareVersion;

	private DeviceKey? _key;

	private DateTime? _keyCreatedAt;

	internal Device(
		TapoClient client,
		IDateTimeProvider dateTimeProvider,
		PhysicalAddress macAddress,
		IPAddress ipAddress,
		string deviceId,
		string alias,
		string hardwareVersion,
		string softwareVersion,
		DeviceKey? key = null,
		DateTime? keyCreatedAt = null
	)
	{
		Client = client;
		_dateTimeProvider = dateTimeProvider;
		MacAddress = macAddress;
		IpAddress = ipAddress;
		DeviceId = deviceId;
		Alias = alias;
		HardwareVersion = hardwareVersion;
		SoftwareVersion = softwareVersion;
		_key = key;
		_keyCreatedAt = keyCreatedAt;
	}

	public string Alias { get; init; }

	public async ValueTask<DeviceKey> Login(CancellationToken cancellationToken = default)
	{
		var now = _dateTimeProvider.GetCurrentDateTime();
		var maxAge = now.Subtract(TimeSpan.FromHours(6));
		if (_keyCreatedAt is { } createdAt && createdAt < maxAge)
		{
			_key = null;
		}

		if (_key is null)
		{
			_key = await Client.DeviceLogin(IpAddress, cancellationToken);
			_keyCreatedAt = now;
		}

		return _key;
	}

	public async Task<DeviceInfo> GetDeviceInfo(DeviceKey deviceKey, CancellationToken cancellationToken = default)
	{
		return await Client.GetDeviceInfo(IpAddress, deviceKey, cancellationToken);
	}
}
