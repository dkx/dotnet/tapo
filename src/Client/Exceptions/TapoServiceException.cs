﻿namespace DKX.Tapo.Client.Exceptions;

public sealed class TapoServiceException : Exception
{
	internal TapoServiceException(string message) : base(message)
	{
	}
}
