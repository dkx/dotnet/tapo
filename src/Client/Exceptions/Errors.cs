﻿using System.Text.Json;

namespace DKX.Tapo.Client.Exceptions;

internal static class Errors
{
	public static void Check(JsonElement data)
	{
		var errorCode = data.GetProperty("error_code").GetInt32();
		if (errorCode == 0)
		{
			return;
		}

		throw errorCode switch
		{
			-1010 => new TapoServiceException("Invalid public key length"),
			-1501 => new TapoServiceException("Invalid request or credentials"),
			-1002 => new TapoServiceException("Incorrect request"),
			-1003 => new TapoServiceException("JSON format error"),
			-10100 => new TapoServiceException("JSON format error"),
			-20601 => new TapoServiceException("Incorrect email or password"),
			-20675 => new TapoServiceException("Cloud token expired or invalid"),
			9999 => new TapoServiceException("Device token expired or invalid"),
			_ => new TapoServiceException($"Unexpected Error Code: {errorCode}")
		};
	}
}
