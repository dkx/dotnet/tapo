﻿using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using NetTools;

namespace DKX.Tapo.Client.Network;

internal static class IpAddressProvider
{
	public static IReadOnlyCollection<IPAddress> GetIpAddresses(bool debug)
	{
		var ranges = GetIpAddressRanges();
		if (debug)
		{
			Console.WriteLine("IP Address ranges:");
			foreach (var range in ranges)
			{
				Console.WriteLine($"  -> {range.Begin} - {range.End}");
			}
		}

		var addresses = new List<IPAddress>();
		foreach (var range in ranges)
		{
			addresses.AddRange(range);
		}

		return addresses;
	}

	private static IReadOnlyCollection<IPAddressRange> GetIpAddressRanges()
	{
		var interfaces = NetworkInterface
			.GetAllNetworkInterfaces()
			.Where(n => n.OperationalStatus == OperationalStatus.Up)
			.Where(n => n.NetworkInterfaceType != NetworkInterfaceType.Loopback)
			.Select(n =>
			{
				var properties = n.GetIPProperties();

				return new
				{
					NetworkInterface = n,
					GatewayAddress = properties
						.GatewayAddresses
						.FirstOrDefault(a => a.Address.AddressFamily == AddressFamily.InterNetwork && Array.FindIndex(a.Address.GetAddressBytes(), b => b != 0) >= 0),
					UnicastAddress = properties
						.UnicastAddresses
						.FirstOrDefault(a => a.Address.AddressFamily == AddressFamily.InterNetwork && Array.FindIndex(a.Address.GetAddressBytes(), b => b != 0) >= 0),
				};
			})
			.Where(d => d.GatewayAddress is not null && d.UnicastAddress is not null)
			.ToArray();

		var ranges = new List<IPAddressRange>();
		foreach (var iface in interfaces)
		{
			var network = IPNetwork.Parse(iface.GatewayAddress!.Address, iface.UnicastAddress!.IPv4Mask);
			var range = new IPAddressRange(network.FirstUsable, network.LastUsable);

			ranges.Add(range);
		}

		return ranges;
	}
}
