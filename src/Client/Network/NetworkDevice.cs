﻿using System.Net;
using System.Net.NetworkInformation;

namespace DKX.Tapo.Client.Network;

public sealed class NetworkDevice
{
	public readonly PhysicalAddress MacAddress;

	public readonly IPAddress IpAddress;

	public NetworkDevice(PhysicalAddress macAddress, IPAddress ipAddress)
	{
		MacAddress = macAddress;
		IpAddress = ipAddress;
	}
}
