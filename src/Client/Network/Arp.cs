﻿using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace DKX.Tapo.Client.Network;

internal static class Arp
{
	public static async Task<IReadOnlyCollection<NetworkDevice>> ReadArp(IReadOnlyCollection<IPAddress> liveAddresses)
	{
		var info = new ProcessStartInfo("arp")
		{
			Arguments = "-a",
			CreateNoWindow = false,
			UseShellExecute = false,
			RedirectStandardOutput = true,
			RedirectStandardError = true,
		};

		using var process = Process.Start(info);
		if (process == null)
		{
			return Array.Empty<NetworkDevice>();
		}

		process.BeginOutputReadLine();
		process.BeginErrorReadLine();

		var data = new List<NetworkDevice>();

		process.OutputDataReceived += (_, args) =>
		{
			if (args.Data is null)
			{
				return;
			}

			var device = MatchArpLine(args.Data);
			if (device is null)
			{
				return;
			}

			if (!liveAddresses.Any(a => a.Equals(device.IpAddress)))
			{
				return;
			}

			data.Add(device);
		};

		await process.WaitForExitAsync();

		return data;
	}

	private static NetworkDevice? MatchArpLine(string line)
	{
		if (Environment.OSVersion.Platform is PlatformID.Win32Windows or PlatformID.Win32NT)
		{
			var parts = new Regex(@"^\s*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s*([a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2}-[a-z0-9]{2})").Match(line);
			if (parts.Success)
			{
				return new NetworkDevice(PhysicalAddress.Parse(parts.Groups[2].Value), IPAddress.Parse(parts.Groups[1].Value));
			}

			return null;
		}

		if (Environment.OSVersion.Platform == PlatformID.Unix)
		{
			var parts = new Regex(@"\((\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\)\s*at\s*([a-z0-9]{2}\:[a-z0-9]{2}\:[a-z0-9]{2}\:[a-z0-9]{2}\:[a-z0-9]{2}\:[a-z0-9]{2})").Match(line);
			if (parts.Success)
			{
				return new NetworkDevice(PhysicalAddress.Parse(parts.Groups[2].Value), IPAddress.Parse(parts.Groups[1].Value));
			}

			return null;
		}

		throw new NotSupportedException($"ArpLookup: platform {Environment.OSVersion.Platform} is not supported");
	}
}
