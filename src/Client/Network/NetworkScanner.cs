﻿using System.Net;
using System.Net.NetworkInformation;

namespace DKX.Tapo.Client.Network;

public sealed class NetworkScanner : INetworkScanner
{
	private readonly bool _debug;

	public NetworkScanner(bool debug = false)
	{
		_debug = debug;
	}

	public async Task<IReadOnlyCollection<NetworkDevice>> Scan(CancellationToken cancellationToken)
	{
		if (_debug)
		{
			Console.Write("Scanning network: ");
		}

		var liveAddresses = new List<IPAddress>();
		await Parallel.ForEachAsync(IpAddressProvider.GetIpAddresses(_debug), new ParallelOptions
		{
			CancellationToken = cancellationToken,
			MaxDegreeOfParallelism = 30,
		}, async (address, _) =>
		{
			var res = await new Ping().SendPingAsync(address);
			if (res.Status == IPStatus.Success)
			{
				liveAddresses.Add(address);

				if (_debug)
				{
					Console.Write("+");
				}
			}
			else if (_debug)
			{
				Console.Write(".");
			}
		});

		if (_debug)
		{
			Console.WriteLine();
		}

		return await Arp.ReadArp(liveAddresses);
	}
}
