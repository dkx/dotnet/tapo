﻿namespace DKX.Tapo.Client.Network;

public interface INetworkScanner
{
	Task<IReadOnlyCollection<NetworkDevice>> Scan(CancellationToken cancellationToken);
}
