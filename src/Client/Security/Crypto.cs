﻿using System.Security.Cryptography;
using System.Text;

namespace DKX.Tapo.Client.Data.Security;

internal sealed class Crypto
{
	public static RsaKeyPair GenerateKeyPair()
	{
		var rsa = RSA.Create(1024);
		return new RsaKeyPair(rsa);
	}

	public static byte[] ReadDeviceKey(string pemKey, RSA rsa)
	{
		return rsa.Decrypt(Convert.FromBase64String(pemKey), RSAEncryptionPadding.Pkcs1);
	}

	public static string Base64Encode(string data)
	{
		return Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
	}

	public static string ShaDigest(string data)
	{
		return Convert.ToHexString(SHA1.HashData(Encoding.UTF8.GetBytes(data)));
	}

	public static string Encrypt(string data, DeviceKey deviceKey)
	{
		using var aes = Aes.Create();
		aes.Key = deviceKey.Key;
		aes.IV = deviceKey.Iv;

		var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
		byte[] encrypted;

		using (var msEncrypt = new MemoryStream())
		{
			using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
			{
				using (var swEncrypt = new StreamWriter(csEncrypt))
				{
					swEncrypt.WriteLine(data);
				}

				encrypted = msEncrypt.ToArray();
			}
		}

		return Convert.ToBase64String(encrypted);
	}

	public static string Decrypt(string data, DeviceKey deviceKey)
	{
		using var aes = Aes.Create();
		aes.Key = deviceKey.Key;
		aes.IV = deviceKey.Iv;

		var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
		var plaintext = "";

		using (var msDecrypt = new MemoryStream(Convert.FromBase64String(data)))
		{
			using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
			{
				using (var srDecrypt = new StreamReader(csDecrypt))
				{
					plaintext = srDecrypt.ReadToEnd();
				}
			}
		}

		return plaintext;
	}

}
