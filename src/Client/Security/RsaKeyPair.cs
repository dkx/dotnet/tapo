﻿using System.Security.Cryptography;
using RSAExtensions;

namespace DKX.Tapo.Client.Data.Security;

internal sealed record RsaKeyPair(RSA Rsa)
{
	public string PublicKey => Rsa.ExportPublicKey(RSAKeyType.Pkcs8, true);

	public string PrivateKey => Rsa.ExportPrivateKey(RSAKeyType.Pkcs8, true);
}
