﻿using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Text.Json;
using DKX.Tapo.Client.Data;
using DKX.Tapo.Client.Data.Security;
using DKX.Tapo.Client.Dates;
using DKX.Tapo.Client.Devices;
using DKX.Tapo.Client.Exceptions;
using DKX.Tapo.Client.Network;
using Polly;
using Polly.Extensions.Http;

namespace DKX.Tapo.Client;

public sealed class TapoClient
{
	private const string BaseUrl = "https://eu-wap.tplinkcloud.com/";

	private readonly INetworkScanner _networkScanner;

	private readonly IDateTimeProvider _dateTimeProvider;

	private readonly HttpClient _http;

	private readonly DeviceFactory _deviceFactory;

	private readonly string _email;

	private readonly string _password;

	private readonly bool _debug;

	private string? _token;

	private DateTime? _tokenCreatedAt;

	public TapoClient(INetworkScanner networkScanner, IDateTimeProvider dateTimeProvider, HttpClient http, string email, string password, bool debug = false)
	{
		_email = email;
		_password = password;
		_debug = debug;
		_networkScanner = networkScanner;
		_dateTimeProvider = dateTimeProvider;
		_http = http;
		_deviceFactory = new DeviceFactory(this, _dateTimeProvider);
	}

	public async Task<IReadOnlyCollection<Device>> GetDeviceList(CancellationToken cancellationToken = default)
	{
		var networkDevices = await _networkScanner.Scan(CancellationToken.None);
		var token = await GetToken(cancellationToken);

		if (_debug)
		{
			Console.WriteLine("Fetching device list...");
		}

		return await SendRequest(
			BaseUrl + "?token=" + token,
			new Dictionary<string, object>
			{
				["method"] = "getDeviceList",
			},
			async (_, data) =>
			{
				var list = data.GetProperty("result").GetProperty("deviceList");
				var devices = new List<Device>();

				foreach (var deviceData in list.EnumerateArray())
				{
					var device = await _deviceFactory.Create(deviceData, networkDevices, cancellationToken);
					if (device is not null)
					{
						devices.Add(device);
					}
				}

				return devices;
			},
			cancellationToken
		);
	}

	public async Task<Device?> CreateDevice(PhysicalAddress macAddress, IPAddress ipAddress, CancellationToken cancellationToken = default)
	{
		return await _deviceFactory.Create(macAddress, ipAddress, cancellationToken);
	}

	private async ValueTask<string> GetToken(CancellationToken cancellationToken)
	{
		var now = _dateTimeProvider.GetCurrentDateTime();
		var maxAge = now.Subtract(TimeSpan.FromHours(6));
		if (_tokenCreatedAt is { } createdAt && createdAt < maxAge)
		{
			_token = null;
		}

		if (_token is null)
		{
			_token = await Login(cancellationToken);
			_tokenCreatedAt = now;
		}

		return _token;
	}

	private async Task<string> Login(CancellationToken cancellationToken)
	{
		if (_debug)
		{
			Console.WriteLine("Logging to Tapo...");
		}

		return await SendRequest(
			BaseUrl,
			new Dictionary<string, object>
			{
				["method"] = "login",
				["params"] = new Dictionary<string, object>
				{
					["appType"] = "Tapo_Android",
					["terminalUUID"] = "59284a9c-e7b1-40f9-8ecd-b9e70c90d19b",
					["cloudUserName"] = _email,
					["cloudPassword"] = _password,
				},
			},
			(_, data) => Task.FromResult(data.GetProperty("result").GetProperty("token").GetString()!),
			cancellationToken
		);
	}

	internal async Task<DeviceKey> DeviceLogin(IPAddress ipAddress, CancellationToken cancellationToken)
	{
		var deviceKey = await DeviceHandshake(ipAddress, cancellationToken);
		return await SecurePassthrough(ipAddress, deviceKey, new Dictionary<string, object>
		{
			["method"] = "login_device",
			["params"] = new Dictionary<string, object>
			{
				["username"] = Crypto.Base64Encode(Crypto.ShaDigest(_email)),
				["password"] = Crypto.Base64Encode(_password),
			},
		}, (_, data) => Task.FromResult(deviceKey with
		{
			Token = data.GetProperty("token").GetString()!,
		}), cancellationToken);
	}

	internal async Task<DeviceInfo> GetDeviceInfo(IPAddress ipAddress, DeviceKey deviceKey, CancellationToken cancellationToken)
	{
		return await SecurePassthrough(ipAddress, deviceKey, new Dictionary<string, object>
		{
			["method"] = "get_device_info",
		}, (_, data) => Task.FromResult(new DeviceInfo(
			data.GetProperty("device_id").GetString()!,
			data.GetProperty("fw_ver").GetString()!,
			data.GetProperty("hw_ver").GetString()!,
			data.GetProperty("type").GetString()!,
			data.GetProperty("model").GetString()!,
			Encoding.UTF8.GetString(Convert.FromBase64String(data.GetProperty("nickname").GetString()!)),
			data.GetProperty("device_on").GetBoolean()
		)), cancellationToken);
	}

	private async Task<DeviceKey> DeviceHandshake(IPAddress ipAddress, CancellationToken cancellationToken)
	{
		var rsa = Crypto.GenerateKeyPair();

		return await SendRequest(
			$"http://{ipAddress}/app",
			new Dictionary<string, object>
			{
				["method"] = "handshake",
				["params"] = new Dictionary<string, object>
				{
					["key"] = rsa.PublicKey,
				},
			},
			(res, data) =>
			{
				var cookieParts = res.Headers.GetValues("Set-Cookie").First().Split(';');
				var session = cookieParts[0];
				var timeout = Convert.ToInt32(cookieParts[1].Split('=')[1]);

				var deviceKey = Crypto.ReadDeviceKey(data.GetProperty("result").GetProperty("key").GetString()!, rsa.Rsa);
				var key = deviceKey.Take(16).ToArray();
				var iv = deviceKey.Skip(16).ToArray();

				return Task.FromResult(new DeviceKey(key, iv, session, TimeSpan.FromSeconds(timeout)));
			},
			cancellationToken
		);
	}

	internal async Task<T> SendRequest<T>(string url, object data, Func<HttpResponseMessage, JsonElement, Task<T>> extract, CancellationToken cancellationToken)
	{
		var policy = HttpPolicyExtensions
			.HandleTransientHttpError()
			.WaitAndRetryAsync(new[]
			{
				TimeSpan.FromSeconds(1),
				TimeSpan.FromSeconds(2),
				TimeSpan.FromSeconds(3),
				TimeSpan.FromSeconds(6),
				TimeSpan.FromSeconds(12),
				TimeSpan.FromSeconds(30),
				TimeSpan.FromSeconds(60),
			}, (result, _) =>
			{
				if (_debug)
				{
					Console.WriteLine($"  -> HTTP request failed ({result.Result.StatusCode}, {result.Exception.Message})");
				}
			});

		var send = new StringContent(JsonSerializer.Serialize(data), Encoding.UTF8, "application/json");
		using var res = await policy.ExecuteAsync(async () => await _http.PostAsync(url, send, cancellationToken));
		using var json = JsonDocument.Parse(await res.Content.ReadAsStringAsync(cancellationToken));
		var root = json.RootElement;

		Errors.Check(root);

		return await extract(res, root);
	}

	internal async Task<T> SecurePassthrough<T>(IPAddress ipAddress, DeviceKey deviceKey, object data, Func<HttpResponseMessage, JsonElement, Task<T>> extract, CancellationToken cancellationToken)
	{
		var securedData = new Dictionary<string, object>
		{
			["method"] = "securePassthrough",
			["params"] = new Dictionary<string, object>
			{
				["request"] = Crypto.Encrypt(JsonSerializer.Serialize(data), deviceKey),
			},
		};

		var send = new StringContent(JsonSerializer.Serialize(securedData), Encoding.UTF8, "application/json");

		var res = await _http.SendAsync(new HttpRequestMessage(HttpMethod.Post, $"http://{ipAddress}/app?token={deviceKey.Token}")
		{
			Content = send,
			Headers =
			{
				{ "Cookie", deviceKey.Session },
			},
		}, cancellationToken);

		using var securedJson = JsonDocument.Parse(await res.Content.ReadAsStringAsync(cancellationToken));
		var securedRoot = securedJson.RootElement;

		Errors.Check(securedRoot);

		using var json = JsonDocument.Parse(Crypto.Decrypt(securedRoot.GetProperty("result").GetProperty("response").GetString()!, deviceKey));
		var root = json.RootElement;

		Errors.Check(root);

		return await extract(res, root.GetProperty("result"));
	}
}
