﻿namespace DKX.Tapo.Client.Data;

public sealed record EnergyUsage(double CurrentPower);
