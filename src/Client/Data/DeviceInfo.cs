﻿namespace DKX.Tapo.Client.Data;

public sealed record DeviceInfo(
	string DeviceId,
	string FirmwareVersion,
	string HardwareVersion,
	string Type,
	string Model,
	string Nickname,
	bool On
);
