﻿namespace DKX.Tapo.Client.Data;

public sealed record DeviceKey(byte[] Key, byte[] Iv, string Session, TimeSpan Timeout)
{
	public string Token { get; init; } = string.Empty;
}
